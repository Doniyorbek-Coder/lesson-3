// Lesson 3
let arr = [];
function fizzBuzz () {
    // your code here
    for(let i=1; i<=100;i++){
       if(i%15===0){
           arr.push('FizzBuzz')
       }else if(i%5===0){
           arr.push('Buzz')
       }else if(i%3===0){
           arr.push('Fizz')
       } else{
           arr.push(i);
    }
  } 
}
fizzBuzz();
console.log(arr);

function filterArray (list) {
    // your code here
    // let formatter = list.filter(el => el.length && typeof el !== 'string' ? true : false )
    // formatter = new Set(formatter.reduce((acc, curr) => [...acc, ...curr], [2, 7,8,9]))
    // return Array.from(formatter).sort((a,b) => b-a )
    // var arr1 = [[2], 23, 'dance', true, [3, 5, 3]],
    // arr2 = [23, 'dance', true],
    // res = arr1.filter(item => !arr2.includes(item));
    // console.log(res);
}
// const fruits = ['apple', 'banana']

// const vegetables = [ ...fruits ] // 0 index
    // console.log(vegetables)
// Output DON'T TOUCH!

const letList = [
    [2], 
    23,
    'dance',
    [7,8,9, 7],
    true, 
    [3, 5, 3],
    [777,855, 9677, 457],
]
console.log(filterArray(letList)) // 2,3,5,7,8,9